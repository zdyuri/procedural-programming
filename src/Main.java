import Part1.Calculations.ArrayUtils1;
import Part1.Calculations.Factorial;
import Part1.Calculations.GCD;
import Part1.Calculations.SCM;
import Part2.*;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

//        // ----------------------------------------------------------
//        //  Smallest common multiple tests
//        // ----------------------------------------------------------
//
//        System.out.println("Smallest common multiple tests");
//        System.out.println(SCM.find(0, 0));
//        System.out.println(SCM.find(1, 0));
//        System.out.println(SCM.find(0, 1));
//        System.out.println(SCM.find(0, 0, 0));
//        System.out.println(SCM.find(2, 7, 14));
//        System.out.println(SCM.find(2, 7, 14, 28));
//
//        // ----------------------------------------------------------
//        //  Greatest common divisor tests
//        // ----------------------------------------------------------
//
//        System.out.println("Greatest common divisor tests");
//        System.out.println(GCD.find(0, 0));
//        System.out.println(GCD.find(5, 0));
//        System.out.println(GCD.find(0, 5));
//        System.out.println(GCD.find(-5, 0));
//        System.out.println(GCD.find(5, 0));
//        System.out.println(GCD.find(8, 16));
//        System.out.println(GCD.find(-2, 16));
//        System.out.println(GCD.find(-8, -16));
//        System.out.println(GCD.find(9, 6, 3, 24));
//        System.out.println(GCD.find(0, 9, 6, 0, 3, 24));
//
//        // ----------------------------------------------------------
//        //  Sum of odd numbers factorials tests
//        // ----------------------------------------------------------
//
//        System.out.println("Sum of odd numbers factorials tests");
//        System.out.println(Factorial.findForOddsTo(3)); // 7
//        System.out.println(Factorial.findForOddsTo(9)); // 368047
//
//        // ----------------------------------------------------------
//        //  Get second greatest number tests
//        // ----------------------------------------------------------
//
//        Integer[] numbers = { 1, 2, 3, 4, 5, 6 };
//        System.out.println(ArrayUtils1.findSecondLargest(numbers));
//
//        Integer[] numbers2 = { 2, 2 };
//        System.out.println(ArrayUtils1.findSecondLargest(numbers2));
////
////        Integer[] numbers3 = { -2, 0 };
//        System.out.println(ArrayUtils1.findSecondLargest(numbers3));
//
//        // ----------------------------------------------------------
//        //  Coprime numbers tests
//        // ----------------------------------------------------------
//
//        System.out.println(Coprime.check(1, 5, 11));
//        System.out.println(Coprime.check(1, 4, 11));
//        System.out.println(Coprime.check(0, 4, 11));
//        System.out.println(Coprime.check(2, 4, 8));
        // ----------------------------------------------------------
        //  Addition of two super big numbers tests
        // ----------------------------------------------------------

//        var first = "8976324598761234978192084";
//        var second = "29347192873490128741980237498712349807";
//        var result = BigIntegersUtils.add(first, second);

        // ----------------------------------------------------------
        //  Armstrong numbers tests
        // ----------------------------------------------------------

//        System.out.println(ArmstrongNumbers.find(371)); // [1, 2, 3, 4, 5, 6, 7, 8, 9, 153, 370, 371]

        // ----------------------------------------------------------
        //  Array's slice sum of elements tests
        // ----------------------------------------------------------

//        var sums = ArrayUtils2.getSums(1, 6); // [1, 2, 3, 4, 5, 6]
//        System.out.println(Arrays.toString(sums)); // [6, 12, 15]

        // ----------------------------------------------------------
        //  Super lock tests
        // ----------------------------------------------------------

//        System.out.println(Arrays.toString(SuperLock.solve()));
    }
}