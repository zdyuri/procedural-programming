package Part1.Calculations;

public class SCM {
    public static long find(long first, long second) {
        if (first == 0 && second == 0) {
            return 0;
        }

        return first / GCD.find(first, second) * second;
    }

    public static long find(long first, long second, long third) {
        long scm = find(first, second);
        return find(scm, third);
    }

    public static long find(long first, long second, long... numbers) {
        long scm = 1;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] != 0) {
                scm = find(scm, numbers[i]);
            }
        }

        return find(first, second, scm);
    }
}
