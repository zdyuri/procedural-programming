package Part1.Calculations;

import java.util.Arrays;

public class ArrayUtils1 {
    public static <T extends Comparable<T>> T findSecondLargest(T[] items) {
        if (items.length < 2) {
            throw new IllegalArgumentException("Array must have at least two elements.");
        }

        T[] itemsCopy = Arrays.copyOf(items, items.length);
        Arrays.sort(itemsCopy);

        return itemsCopy[itemsCopy.length - 2];
    }
}
