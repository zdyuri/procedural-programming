package Part1.Calculations;

public class Factorial {
    public static long findForOddsTo(long max) {
        long factorial = 1;
        long accumulator = 1;

        for (int i = 3; i <= max; i += 2) {
            factorial = factorial * i * (i - 1);
            accumulator += factorial;
        }

        return accumulator;
    }
}
