package Part1.Calculations;

public class GCD {
    public static long find(long first, long second) {
        first = Math.abs(first);
        second = Math.abs(second);

        if (first == 0) {
            return second;
        }

        if (second == 0) {
            return first;
        }

        if (first == second) {
            return first;
        }

        long greatestNumber = Math.max(first, second);
        long smallestNumber = Math.min(first, second);
        while (smallestNumber > 0) {
            long reminder = greatestNumber % smallestNumber;
            greatestNumber = smallestNumber;
            smallestNumber = reminder;
        }

        return greatestNumber;
    }

    public static long find(long first, long second, long third) {
        long gcd = find(first, second);
        return find(gcd, third);
    }

    public static long find(long first, long second, long... numbers) {
        long gcd = 0;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] != 0) {
                gcd = find(gcd, numbers[i]);
            }
        }

        return find(first, second, gcd);
    }
}
