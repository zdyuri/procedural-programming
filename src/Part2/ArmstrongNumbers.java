package Part2;

import java.util.ArrayList;
import java.util.List;

public class ArmstrongNumbers {
    public static List<Integer> find(int max) {
        List<Integer> result = new ArrayList<>();

        for (int i = 1; i <= max; i++) {
            int digitsSum = getPoweredDigitsSum(i);

            if (digitsSum == i) {
                result.add(digitsSum);
            }
        }

        return result;
    }

    private static int getPoweredDigitsSum(int number) {
        int result = 0;
        int numberLength = getNumberLength(number);

        while(number > 0) {
            int currentDigit = number % 10;
            result += Math.pow(currentDigit, numberLength);
            number /= 10;
        }

        return result;
    }

    private static int getNumberLength(int number) {
        return (int)Math.ceil(Math.log10(number));
    }
}
