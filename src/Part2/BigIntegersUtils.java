package Part2;

import java.math.BigInteger;

public class BigIntegersUtils {
    public static String add(String first, String second) {
        if (first.length() > second.length()) {
            String temp = first;
            first = second;
            second = temp;
        }

        String str = "";

        int firstStringLength = first.length();
        int secondStringLength = second.length();

        first = new StringBuilder(first).reverse().toString();
        second = new StringBuilder(second).reverse().toString();

        int carry = 0;
        for (int i = 0; i < firstStringLength; i++) {
            int sum = ((first.charAt(i) - '0') + (second.charAt(i) - '0') + carry);

            str += (char)(sum % 10 + '0');

            carry = sum / 10;
        }

        for (int i = firstStringLength; i < secondStringLength; i++) {
            int sum = ((second.charAt(i) - '0') + carry);

            str += (char)(sum % 10 + '0');

            carry = sum / 10;
        }

        if (carry > 0) {
            str += (char)(carry + '0');
        }

        return new StringBuilder(str).reverse().toString();
    }
}
