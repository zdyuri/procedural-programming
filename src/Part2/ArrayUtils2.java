package Part2;

public class ArrayUtils2 {
    private static final int GENERATED_ARRAY_SIZE = 6;

    public static int[] getSums(int from, int to) {
        assertParams(from, to);

        var array = generateArray(from, to);

        return getSumsCore(array);
    }

    private static int[] generateArray(int from, int to) {
        int[] array = new int[GENERATED_ARRAY_SIZE];

        int currentElementIndex = 0;
        while(currentElementIndex < GENERATED_ARRAY_SIZE) {
            array[currentElementIndex++] = from == to ? from : from++;
        }

        return array;
    }

    private static void assertParams(int from, int to) {
        if (from > to) {
            throw new IllegalArgumentException("From must be less than or equal to to");
        }
    }

    private static int[] getSumsCore(int[] array) {
        return new int[] {
            getSumCore(array, 0, 2),
            getSumCore(array, 2, 4),
            getSumCore(array, 3, 5)
        };
    }

    private static int getSumCore(int[] array, int startIndex, int endIndex) {
        int result = 0;

        while(startIndex <= endIndex) {
            result += array[startIndex++];
        }

        return result;
    }
}