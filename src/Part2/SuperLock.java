package Part2;

import java.util.Random;

public class SuperLock {
    private static final int MAX_DICES_VALUES_SUM = 10;
    private static final int LOCK_SIZE = 10;
    private static final int MAX_DICE_VALUE = 6;
    private static final int WINDOW_SIZE = 3;

    public static int[] solve() {
        int[] cells = new int[LOCK_SIZE];

        initializeCells(cells);

        for (int i = 0; i <= LOCK_SIZE - WINDOW_SIZE; i++) {
            cells[i + WINDOW_SIZE - 1] = getNumberToInsert(cells, i);
        }
        
        return cells;
    }

    private static int getNumberToInsert(int[] cells, int i) {
        int diff = MAX_DICES_VALUES_SUM - (cells[i] + cells[i + 1]);
        return diff > MAX_DICE_VALUE ? MAX_DICE_VALUE : diff;
    }

    private static void initializeCells(int[] cells) {
        Random random = new Random();

        cells[0] = normalizeCellValue(random.nextInt(MAX_DICE_VALUE));
        cells[1] = normalizeCellValue(random.nextInt(MAX_DICE_VALUE));
    }

    private static int normalizeCellValue(int value) {
        return value == 0 ? 1 : value;
    }
}
