package Part2;

import Part1.Calculations.GCD;

public class Coprime {
    public static boolean check(int first, int second, int third) {
        return GCD.find(first, second, third) == 1;
    }
}
